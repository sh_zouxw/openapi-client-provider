/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 13:42 创建
 */
package com.acooly.module.openapi.client.provider.cmb.partner.impl;

import com.acooly.module.openapi.client.provider.cmb.CmbProperties;
import com.acooly.module.openapi.client.provider.cmb.partner.CmbPartnerIdLoadManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 此为获取partnerId的空实现 （demo）
 *
 * @author zhangpu 2017-11-15 13:42
 */
@Slf4j
@Service("CmbPartnerIdLoadManager")
public class CmbEmptyPartnerIdLoader implements CmbPartnerIdLoadManager {

    @Autowired
    private CmbProperties cmbProperties;

    @Override
    public String load(String orderNo) {
        return cmbProperties.getPartnerId();
    }
}
