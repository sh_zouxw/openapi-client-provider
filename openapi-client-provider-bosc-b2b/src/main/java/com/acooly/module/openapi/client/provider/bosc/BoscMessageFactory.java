package com.acooly.module.openapi.client.provider.bosc;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.api.message.MessageMeta;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponseDomain;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceEnum;
import com.acooly.module.openapi.client.provider.bosc.message.response.ActivateAcctResponse;
import com.acooly.module.openapi.client.provider.bosc.message.response.OpenAcctResponse;
import com.acooly.module.openapi.client.provider.bosc.message.response.QueryAcctStatusResponse;
import com.acooly.module.openapi.client.provider.bosc.message.response.QueryVirAcctBalanceResponse;
import com.acooly.module.openapi.client.provider.bosc.message.response.UpBindCardResponse;
import com.acooly.module.openapi.client.provider.bosc.message.response.VirAcctInResponse;
import com.google.common.collect.Maps;

@Service
public class BoscMessageFactory implements MessageFactory {

	public static Map<String, MessageMeta> metas = Maps.newHashMap();

	static {
		metas.put(BoscServiceEnum.openAcct.getKey(), new MessageMeta(OpenAcctResponse.class));
		metas.put(BoscServiceEnum.activateAcct.getKey(), new MessageMeta(ActivateAcctResponse.class));
		metas.put(BoscServiceEnum.queryAcctStatus.getKey(), new MessageMeta(QueryAcctStatusResponse.class));
		metas.put(BoscServiceEnum.queryVirAcctBalance.getKey(), new MessageMeta(QueryVirAcctBalanceResponse.class));
		metas.put(BoscServiceEnum.upBindCard.getKey(), new MessageMeta(UpBindCardResponse.class));
		metas.put(BoscServiceEnum.virAcctIn.getKey(), new MessageMeta(VirAcctInResponse.class));
	}

	@Override
	public ApiMessage getRequest(String serviceName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ApiMessage getResponse(String serviceName) {
		if (metas.get(serviceName) == null || metas.get(serviceName).getResponse() == null) {
			return newInstance(BoscResponseDomain.class);
		} else {
			return newInstance(metas.get(serviceName).getResponse());
		}
	}

	@Override
	public ApiMessage getNotify(String serviceName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ApiMessage getReturn(String serviceName) {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T newInstance(Class<T> clazz) {
		try {
			return (T) clazz.newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException("InstantiationException:" + clazz);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("IllegalAccessException:" + clazz);
		}
	}

}
