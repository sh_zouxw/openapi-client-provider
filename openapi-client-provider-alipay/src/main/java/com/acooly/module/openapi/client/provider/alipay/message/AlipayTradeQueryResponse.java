package com.acooly.module.openapi.client.provider.alipay.message;

import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayApiMsgInfo;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayResponse;
import com.acooly.module.openapi.client.provider.alipay.enums.AlipayServiceEnum;
import com.acooly.module.openapi.client.provider.alipay.support.AlipayAlias;

import lombok.Getter;
import lombok.Setter;

@AlipayApiMsgInfo(service=AlipayServiceEnum.PAY_ALIPAY_TRADE_APP,type=ApiMessageType.Response)
@Setter
@Getter
public class AlipayTradeQueryResponse extends AlipayResponse{

	/**
	 * 交易状态
	 */
	@AlipayAlias(value = "trade_status")
	private String tradeStatus;
	
	/** 订单总金额，与请求中的订单金额一致 */
	@AlipayAlias(value = "total_amount")
	private String totalAmount;
	
	/** 实收金额，商户实际入账的金额（扣手续费之前） */
	@AlipayAlias(value = "receipt_amount")
	private String receiptAmount;
	
	/** 打款给卖家的时间 */
	@AlipayAlias(value = "send_pay_date")
	private String sendPayDate;
}
