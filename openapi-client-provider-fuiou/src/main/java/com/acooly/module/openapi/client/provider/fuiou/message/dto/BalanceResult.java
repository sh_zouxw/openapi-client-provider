/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月5日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.message.dto;

import com.acooly.core.utils.ToString;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhangpu
 */
@XStreamAlias("result")
public class BalanceResult {

	@NotEmpty
	@Size(min = 1, max = 20)
	@XStreamAlias("user_id")
	private String userId;

	/** 总余额 */
	@Size(min = 1, max = 10)
	@XStreamAlias("ct_balance")
	private String ctBalance;

	/** 可用余额 */
	@Size(min = 1, max = 10)
	@XStreamAlias("ca_balance")
	private String caBalance;

	/** 冻结余额 */
	@Size(min = 1, max = 10)
	@XStreamAlias("cf_balance")
	private String cfBalance;

	/** 未转结余额 */
	@Size(min = 1, max = 10)
	@XStreamAlias("cu_balance")
	private String cuBalance;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCtBalance() {
		return ctBalance;
	}

	public void setCtBalance(String ctBalance) {
		this.ctBalance = ctBalance;
	}

	public String getCaBalance() {
		return caBalance;
	}

	public void setCaBalance(String caBalance) {
		this.caBalance = caBalance;
	}

	public String getCfBalance() {
		return cfBalance;
	}

	public void setCfBalance(String cfBalance) {
		this.cfBalance = cfBalance;
	}

	public String getCuBalance() {
		return cuBalance;
	}

	public void setCuBalance(String cuBalance) {
		this.cuBalance = cuBalance;
	}

	@Override
	public String toString() {
		return ToString.toString(this);
	}

}
