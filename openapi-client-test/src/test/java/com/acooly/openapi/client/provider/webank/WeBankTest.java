package com.acooly.openapi.client.provider.webank;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.webank.WeBankApiService;
import com.acooly.module.openapi.client.provider.webank.message.WeBankDeductQueryRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankDeductQueryResponse;
import com.acooly.module.openapi.client.provider.webank.message.WeBankDeductRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankDeductResponse;
import com.acooly.module.openapi.client.provider.webank.message.WeBankSignRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankSignResponse;
import com.acooly.module.openapi.client.provider.webank.message.WeBankSmsRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankSmsResponse;
import com.acooly.module.openapi.client.provider.webank.message.WeBankWithdrawQueryRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankWithdrawQueryResponse;
import com.acooly.module.openapi.client.provider.webank.message.WeBankWithdrawRequest;
import com.acooly.module.openapi.client.provider.webank.message.WeBankWithdrawResponse;
import com.acooly.module.openapi.client.provider.webank.message.dto.WeBankDeductInfo;
import com.acooly.module.openapi.client.provider.webank.message.dto.WeBankQueryInfo;
import com.acooly.module.openapi.client.provider.webank.message.dto.WeBankSignInfo;
import com.acooly.module.openapi.client.provider.webank.message.dto.WeBankSmsInfo;
import com.acooly.module.openapi.client.provider.webank.message.dto.WeBankWithdrawInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "WeBankTest")
public class WeBankTest extends NoWebTestBase {

    @Autowired
    private WeBankApiService weBankApiService;

    /**
     * BankSms:微众绑卡申请
     */
    @Test
    public void testBankSms() {

        WeBankSmsRequest request = new WeBankSmsRequest();
        //组装报文
        WeBankSmsInfo order = new WeBankSmsInfo();

        order.setMerId("104100070118456");
        order.setOrderId(Ids.oid());
        order.setBizType("11");
        order.setPhoneNo("18202393992");
        order.setBusinessName("傅39");
        request.setWeBankSmsInfo(order);

        try {
        WeBankSmsResponse response = weBankApiService.weBankSms(request);
        System.out.println("微众绑卡申请测试执行结果：" + JSON.toJSONString(response));

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

    /**
     * BankSign:微众签约
     */
    @Test
    public void testBankSign() {

        WeBankSignRequest request = new WeBankSignRequest();
        //组装报文
        WeBankSignInfo order = new WeBankSignInfo();
        order.setOrderId(Ids.oid());
        order.setBizType("09");
        order.setAcctNo("6222023801233290000");
        order.setAcctName("傅39");
        order.setPhoneNo("18202393992");
        order.setCertType("01");
        order.setCertId("500223199111086316");
        order.setSmsCode("123456");
        order.setSmsOrderId("o1712011009342360001");
        request.setWeBankSignInfo(order);
        try {
            WeBankSignResponse response = weBankApiService.weBankSign(request);
            System.out.println("微众签约测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

    /**
     * BankDeduct:微众代扣
     */
    @Test
    public void testBankDeduct(){

        WeBankDeductRequest request = new WeBankDeductRequest();
        //组装报文
        WeBankDeductInfo order = new WeBankDeductInfo();
        order.setOrderId(Ids.oid());
        order.setBizType("02");
        order.setAcctNo("6222023801233290000");
        order.setAcctName("傅39");
        order.setAmount("6.33");
        order.setIdType("01");
        order.setIdNum("500223199111086316");
        order.setAcctFlag("11");
        order.setCardType("01");
        order.setPhoneNo("18202393992");


        request.setWeBankDeductInfo(order);
        try {
            WeBankDeductResponse response = weBankApiService.weBankDeduct(request);
            System.out.println("微众代扣测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

    /**
     * BankDeduct:微众代付
     */
    @Test
    public void testBankWithdraw(){

        WeBankWithdrawRequest request = new WeBankWithdrawRequest();
        //组装报文
        WeBankWithdrawInfo order = new WeBankWithdrawInfo();
        order.setMerId("104100070118456");
        order.setBankNo("104100000004");
        order.setOrderId(Ids.oid());
        order.setBizType("01");
        order.setAcctNo("6226090000000048");
        order.setAcctName("张三");
        order.setAmount("6.66");
        order.setAcctFlag("12");
        order.setCardType("01");
        order.setPhoneNo("18100000000");
        request.setWeBankWithdrawInfo(order);
        try {
            WeBankWithdrawResponse response = weBankApiService.weBankWithdraw(request);
            System.out.println("微众代扣测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

    /**
     * BankDeductQuery:微众代付查询
     */
    @Test
    public void testBankWithdrawQuery(){

        WeBankWithdrawQueryRequest request = new WeBankWithdrawQueryRequest();
        //组装报文
        WeBankQueryInfo order = new WeBankQueryInfo();
        order.setMerId("104100070118456");
        order.setOrderId("o17121317303477460003");
        order.setBizType("10");
        order.setOrigBizType("01");//代扣
        request.setWeBankQueryInfo(order);
        try {
            WeBankWithdrawQueryResponse response = weBankApiService.weBankWithdrawQuery(request);
            System.out.println("微众代扣查询测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

    /**
     * BankDeductQuery:微众代扣查询
     */
    @Test
    public void testBankDeductQuery(){

        WeBankDeductQueryRequest request = new WeBankDeductQueryRequest();
        //组装报文
        WeBankQueryInfo order = new WeBankQueryInfo();
        order.setMerId("104100070118456");
        order.setOrderId("o17121317303477460003");
        order.setBizType("10");
        order.setOrigBizType("02");//代扣
        request.setWeBankQueryInfo(order);
        try {
            WeBankDeductQueryResponse response = weBankApiService.weBankDeductQuery(request);
            System.out.println("微众代扣查询测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }
}

