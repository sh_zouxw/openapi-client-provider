package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPResponse;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/4/13 14:29
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_PAY_UNBIND_CARD,type = ApiMessageType.Response)
public class BaoFuProtocolPayUnBindCardResponse extends BaoFuPResponse {

}
