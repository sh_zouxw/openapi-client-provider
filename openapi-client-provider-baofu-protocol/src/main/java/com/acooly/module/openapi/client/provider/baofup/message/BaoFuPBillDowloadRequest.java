package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPRequest;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/1/30 17:43
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.BILL_DOWNLOAD, type = ApiMessageType.Request)
public class BaoFuPBillDowloadRequest extends BaoFuPRequest {

    /**
     * 文件类型 收单：fi  出款：fo
     */
    @NotBlank(message = "文件类型不能为空")
    @BaoFuPAlias(value = "file_type")
    private String fileType;


    /**
     * 客户端IP 商户下载文件所属IP须向宝付报备
     */
    @NotBlank(message = "客户端IP不能为空")
    @BaoFuPAlias(value = "client_ip")
    private String clientIp;


    /**
     * 清算日期 对账单日期
     * 格式：yyyy-mm-dd
     */
    @NotBlank(message = "服务版本不能为空")
    @BaoFuPAlias(value = "settle_date")
    private String settleDate;

    /**
     * 文件路径
     */
    @BaoFuPAlias(value = "filePath", sign = false, request = false)
    private String filePath;

    /**
     * 文件名称，如果不传默认为：账期.text
     */
    @BaoFuPAlias(value = "fileName", sign = false, request = false)
    private String fileName;

    /**
     * 文件后缀
     */
    @BaoFuPAlias(value = "fileSuffix", sign = false, request = false)
    private String fileSuffix = "zip";
}
