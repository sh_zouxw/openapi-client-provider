<!-- title: 金运通支付SDK组件 -->
<!-- type: gateway -->
<!-- author: zhike -->

## 1. 提供能力

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 2. 使用说明

```xml
<dependency>
  <groupId>com.acooly</groupId>
  <artifactId>openapi-client-provider-jyt</artifactId>
  <version>4.2.0-SNAPSHOT</version>
</dependency>
```

直接采用金运通的SDK，然后扩展异步通知处理
使用方式：
1、在自己工程注入JytApiService

2、此sdk提供银盛的支付请求鉴权（发送验证码）、支付消费交易、重新获取验证码、订单查询、异步通知、对账下载

3、密钥加载支持方式
   a：通过配置文件配置密钥加载路径；

4、本sdk中实名支付的异步通知地址固定值，所以调用的时候不需要传入notify_url：请参考com.acooly.module.openapi.client.provider.jyt.OpenAPIClientJytConfigration.getApiSDKServlet
   a:实名支付：/gateway/notify/jytNotify/deductPay
   接收异步通知的时候实现接口NotifyHandler，重新handleNotify和serviceKey方法，其中derviceKey方法需要绑定的key值在枚举JytServiceEnum可以获取到
   快捷支付异步通知接收示例：
   @Slf4j
   @Service
   public class YsFastPayNotifyService implements NotifyHandler{

       @Autowired
       private GatewayDeductTransactionService gatewayDeductTransactionService;

       @Override
       public void handleNotify(ApiMessage notify) {
           JytFastPayNotify fastPayNotify = (JytFastPayNotify)notify;
           log.info("银盛快捷异步通知报文：{}", JSON.toJSONString(fastPayNotify));
           String orderNo = fastPayNotify.getOut_trade_no();
           GatewayDeductTransaction deductTransaction = gatewayDeductTransactionService.findEntityBysettleSerialNo(orderNo);
           MDC.put("gid", deductTransaction.getGid());
           log.info("银盛快捷支付订单orderNo={}异步处理开始", orderNo);
           if (deductTransaction == null) {
               log.info("此笔流水bizOrderNo={}不存在", orderNo);
               return;
           } else if (deductTransaction.getStatus() == MusGwStatusEnum.BS || deductTransaction.getStatus() == MusGwStatusEnum.BF) {
               log.info("此笔流水bizOrderNo={}已处理", orderNo);
               return;
           }
           deductTransaction.setSettleDate(Dates.parse(fastPayNotify.getAccount_date(), "yyyyMMdd"));
           deductTransaction.setSucAmount(Money.amout(fastPayNotify.getTotal_amount()).getCent());
           deductTransaction.setNotifyMessage(JSON.toJSONString(fastPayNotify));
           if (Strings.equals(fastPayNotify.getTrade_status(), "TRADE_SUCCESS")||Strings.equals(fastPayNotify.getTrade_status(), "TRADE_FINISHED")) {
               deductTransaction.setStatus(MusGwStatusEnum.BS);
           } else if(Strings.equals(fastPayNotify.getTrade_status(), "TRADE_CLOSED")||
                   Strings.equals(fastPayNotify.getTrade_status(), "TRADE_SUCCESS")||
                   Strings.equals(fastPayNotify.getTrade_status(), "TRADE_SUCCESS")) {
               deductTransaction.setStatus(MusGwStatusEnum.BF);
           } else {
               log.info("银盛快捷交易状态trade_status={}不明，不处理",fastPayNotify.getTrade_status());
           }
           gatewayDeductTransactionService.update(deductTransaction);

       }

       @Override
       public String serviceKey() {
           return JytServiceEnum.FAST_PAY.getKey();
       }
   }

5、接口的服务码和对应的唯一表示统一放在com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum

注意：调用接口的时候除业务参数以外其它参数不需要手动传入，如果是配置文件方式partnerId也不用传，charset默认为UTF-8
     timestamp默认为当前时间,配置文件方式不用传入请求地址

7、sdk提供了单独签名、验签、异步实体转化方法

8、一些接口需要传入business_code和seller_name,这两个参数会放到配置文件中，使用的时候请配置好，注入OpenAPIClientJytProperties后获取，
    也可以在自己的业务系统管理，然后传入

9、配置文件示例：
acooly.openapi.client.jyt.enable=true
acooly.openapi.client.jyt.gatewayUrl=https://test.jytpay.com/JytRNPay/tranCenter/encXmlReq.do
acooly.openapi.client.jyt.publicKeyPath=classpath:keystore/jyt/public.cer
acooly.openapi.client.jyt.privateKeyPath=classpath:keystore/jyt/private.pfx
acooly.openapi.client.jyt.privateKeyPassword=password
acooly.openapi.client.jyt.domain=http://218.70.106.250:8881
acooly.openapi.client.jyt.connTimeout=10000
acooly.openapi.client.jyt.readTimeout=30000
acooly.openapi.client.jyt.partnerId=301060120002
acooly.openapi.client.jyt.notifyUrlPrefix=/
