package com.acooly.module.openapi.client.provider.cj.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.cj.domain.CjApiMsgInfo;
import com.acooly.module.openapi.client.provider.cj.domain.CjRequest;
import com.acooly.module.openapi.client.provider.cj.enums.CjServiceEnum;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@CjApiMsgInfo(service = CjServiceEnum.CJ_REAL_DEDUCT, type = ApiMessageType.Request)
public class CjRealDeductRequest extends CjRequest {

    /** 用户ID */
    private String userId;
    /** 业务代码, 接入生产前，业务人员会提供 */
    private String businessCode;
    /** 产品编码, 接入生产前，业务人员会提供 */
    private String productCode;
    /** 企业账号 */
    private String corpAccNo;
    /** 对公对私 */
    private String accountProp;
    /** 银行通用名称 */
    private String bankGeneralName;
    /** 账号类型 */
    private String accountType;
    /** 账号 */
    private String accountNo;
    /** 账户名称 */
    private String accountName;
    /** 开户行所在省 */
    private String province;
    /** 开户行所在市 */
    private String city;
    /** 开户行名称 如：中国建设银行广州东山广场分理处*/
    private String bankName;
    /** 开户行号， 对方账号对应的开户行支行行号 */
    private String bankCode;
    /** 清算行号 */
    private String drctBankCode;

    /** 货币类型, 人民币 */
    private String currency = "CNY";
    /** 金额 */
    private String amount;
    /** 开户证件类型 */
    private String idType="0";
    /** 证件号 */
    private String id;
    /** 手机号 */
    private String tel;
    /** 外部企业流水号 */
    private String corpFlowNo;
    /** 备注 */
    private String summary;
    /** 用途 */
    private String postscript;
    /** 订单状态 */
    private String status;
    /** 订单信息描述 */
    private String detail;

}
