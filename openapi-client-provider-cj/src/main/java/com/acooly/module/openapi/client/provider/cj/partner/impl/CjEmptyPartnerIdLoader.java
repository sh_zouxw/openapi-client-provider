/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 13:42 创建
 */
package com.acooly.module.openapi.client.provider.cj.partner.impl;


import com.acooly.module.openapi.client.provider.cj.CjProperties;
import com.acooly.module.openapi.client.provider.cj.partner.CjPartnerIdLoadManager;
import com.acooly.module.openapi.client.provider.cj.domain.CjResponse;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 此为获取partnerId的空实现 （demo）
 *
 * @author zhangpu 2017-11-15 13:42
 */
@Slf4j
@Service("cjPartnerIdLoadManager")
public class CjEmptyPartnerIdLoader implements CjPartnerIdLoadManager {

    @Autowired
    private CjProperties cjProperties;

    @Override
    public String load(CjResponse cjResponse) {
        String partnerId = "";
        if(StringUtils.isBlank(cjResponse.getBatchNo())){
            //如果是批量业务，查询此批次的第一条流水的partnerId
        }else{

        }
        return cjProperties.getMertid();
    }
}
