package com.acooly.module.openapi.client.provider.yibao.message;

import com.acooly.core.utils.Dates;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoRequest;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author zhike 2018/6/26 14:37
 */
@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDPAY_REQUEST,type = ApiMessageType.Request)
public class YibaoSmsBindPayApplyRequest extends YibaoRequest {
    /**
     * 计费商编
     */
    @Size(max = 32)
    @YibaoAlias(value = "csmerchantno")
    private String csMerchantNo;

    /**
     * 用户标识
     * 商户生成的用户唯一标识
     */
    @Size(max = 32)
    @NotBlank
    @YibaoAlias(value = "identityid")
    private String identityId;

    /**
     * 用户标识类型
     * MAC：网卡地址
     * IMEI：国际移动设备标识
     * ID_CARD：用户身份证号
     * PHONE：手机号
     * EMAIL：邮箱
     * USER_ID：用户 id
     * AGREEMENT_NO：用户纸质订单协
     * 议号
     */
    @Size(max = 32)
    @NotBlank
    @YibaoAlias(value = "identitytype")
    private String identityType;

    /**
     * 请求时间
     * 格式：yyyy-MM-dd HH:mm:ss
     */
    @NotBlank
    @YibaoAlias(value = "requesttime")
    private String requestTime = Dates.format(new Date(), Dates.CHINESE_DATETIME_FORMAT_LINE);

    /**
     * 单位：元
     * 精确到两位小数
     * 大于等于 0.01
     */
    @NotBlank
    @Size(max = 16)
    @YibaoAlias(value = "amount")
    private String amount;

    /**
     * 商品名称
     */
    @NotBlank
    @Size(max = 256)
    @YibaoAlias(value = "productname")
    private String productName;

    /**
     * 有效期
     * 单位：分钟。
     * 若不传则默认有效期 24h
     * 传的值要大于等于 1min，小于等于 7 天
     * 传的值若小于 1min 系统置为 1min
     * 传的值若大于 7 天系统置为 7 天
     */
    @Size(max = 16)
    @YibaoAlias(value = "avaliabletime")
    private String avaliableTime;

    /**
     * 卡号前六位
     */
    @NotBlank
    @Size(max = 16)
    @YibaoAlias(value = "cardtop")
    private String cardTop;

    /**
     * 卡号后四位
     */
    @NotBlank
    @Size(max = 16)
    @YibaoAlias(value = "cardlast")
    private String cardLast;

    /**
     * 终端号
     * 非必填，若商户有需求请与技术支持人
     * 员联系，且需开通相关权限。
     */
    @Size(max = 16)
    @YibaoAlias(value = "terminalno")
    private String terminalNo;

    /**
     * 注册硬件终端标识码
     */
    @NotBlank
    @Size(max = 16)
    @YibaoAlias(value = "terminalid")
    private String terminalId;

    /**
     * 用户注册时间
     * 格式：yyyy-MM-dd HH:mm:ss
     */
    @NotBlank
    @Size(max = 16)
    @YibaoAlias(value = "registtime")
    private String registTime;

    /**
     * 用户注册 IP
     * 此参数为非必填参数，当前若不填此参
     * 数，接口传参中就不要带有此参数信息，
     * 暂不支持带有此参数而参数值是空字符
     * 串的形式
     */
    @Size(max = 64)
    @YibaoAlias(value = "registip")
    private String registIp;

    /**
     * 上一次登录 IP
     */
    @Size(max = 64)
    @YibaoAlias(value = "lastloginip")
    private String lastLoginIp;

    /**
     * 上一次登录时间
     * 格式：yyyy-MM-dd HH:mm:ss
     */
    @Size(max = 32)
    @YibaoAlias(value = "lastlogintime")
    private String lastLoginTime;

    /**
     * 上一次登录硬件终端标
     * 识码
     */
    @NotBlank
    @Size(max = 16)
    @YibaoAlias(value = "lastloginterminalid")
    private String lastLoginTerminalId;

    /**
     * 是否设置支付密码
     * 0：否
     * 1：是
     */
    @NotBlank
    @Size(max = 16)
    @YibaoAlias(value = "issetpaypwd")
    private String issetPayPwd;

    /**
     * 扩展字段
     */
    @Size(max = 42)
    @YibaoAlias(value = "free1")
    private String freeOne;

    /**
     * 扩展字段
     */
    @Size(max = 42)
    @YibaoAlias(value = "free2")
    private String freeTwo;

    /**
     * 扩展字段
     */
    @Size(max = 42)
    @YibaoAlias(value = "free3")
    private String freeThree;

}