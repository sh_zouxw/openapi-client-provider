package com.acooly.module.openapi.client.provider.yibao.message;

import com.acooly.core.utils.Dates;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoRequest;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author zhike 2018/6/26 14:37
 */
@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDPAY_CONFIRM,type = ApiMessageType.Request)
public class YibaoSmsBindPayConfirmRequest extends YibaoRequest {
    /**
     * 短验码
     * 短信或语音验证码，6 位数字
     */
    @Size(max = 16)
    @YibaoAlias(value = "validatecode")
    private String smsCode;

}