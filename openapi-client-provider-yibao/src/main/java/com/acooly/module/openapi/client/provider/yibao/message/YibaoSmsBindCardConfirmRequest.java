package com.acooly.module.openapi.client.provider.yibao.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoRequest;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/6/26 13:56
 */
@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDCARD_CONFIRM,type = ApiMessageType.Request)
public class YibaoSmsBindCardConfirmRequest extends YibaoRequest {

    /**
     * 短验码
     * 短信或语音验证码，6 位数字
     */
    @Size(max = 16)
    @YibaoAlias(value = "validatecode")
    private String smsCode;

}
