package com.acooly.module.openapi.client.provider.yinsheng.message.dto;

import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengAppPayBankTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/9 10:08
 */
@Getter
@Setter
public class YinShengAppPayInfo extends YinShengInfo{

    /**
     * 银行代码（默认为支付宝APP支付）
     * SDK支付所支持的类型，微信-1902000 支付宝-1903000
     */
    private String bank_type = YinShengAppPayBankTypeEnum.ALI_APP_PAY.getCode();

    /**
     * 业务代码
     * 终端设备号
     */
    private String device_info;

    /**
     * 限制信用卡
     * 是否限制信用卡。值为1表示禁用信用卡，0或为空表示不限制，只针对banktype为微信-1902000时生效
     */
    private String limit_credit_pay;

    /**
     * 公众账号ID
     * 商户app对应的微信开发平台移动应用APPID，sdk直连模式必填
     */
    private String appid;
}
