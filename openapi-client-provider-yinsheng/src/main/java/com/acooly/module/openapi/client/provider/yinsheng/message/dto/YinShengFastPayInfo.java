package com.acooly.module.openapi.client.provider.yinsheng.message.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

@Getter
@Setter
public class YinShengFastPayInfo implements Serializable {

    /**
     * 店铺时间YYYYMMdd
     */
    @NotBlank
    private String shopdate;
    /**
     * 商户订单号
     */
    @NotBlank
    private String out_trade_no;
    /**
     * 商品描述
     */
    @NotBlank
    private String subject;
    /**
     * 订单总金额
     */
    @NotBlank
    private String total_amount;
    /**
     * 未付款时间
     */
    @NotBlank
    private String timeout_express;
    /**
     * 业务代码
     */
    @NotBlank
    private String business_code;
    /**
     * 商户号
     */
    @NotBlank
    private String seller_id;
    /**
     * 商户名
     */
    @NotBlank
    private String seller_name;
    /**
     * 银行姓名
     */
    @NotBlank
    private String buyer_name;
    /**
     * 银行帐号
     */
    @NotBlank
    private String buyer_card_number;
    /**
     * 手机号码
     */
    @NotBlank
    private String buyer_mobile;

    /**
     * 银行类型
     */
    @NotBlank
    private String bank_type;
    /**
     * 付款方银行账户类型
     */
    @NotBlank
    private String bank_account_type;
    /**
     * 银行卡类型
     */
    @NotBlank
    private String support_card_type;
    /**
     * 银行名称
     */
    @NotBlank
    private String bank_name;
    /**
     * 证件类型
     */
    @NotBlank
    private String pyerIDTp = "01";
    /**
     * 身份证
     */
    @NotBlank
    private String pyerIDNo;
}
