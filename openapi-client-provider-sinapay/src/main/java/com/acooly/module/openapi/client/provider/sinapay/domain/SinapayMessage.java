/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-22 09:22 创建
 */
package com.acooly.module.openapi.client.provider.sinapay.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.api.util.ApiClientUtils;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 富滇银行存管基础报文
 *
 * @author zhike 2017-09-22 09:22
 */
public class SinapayMessage implements ApiMessage {

    @NotEmpty
    @ApiItem
    private String service = ApiClientUtils.getServiceName(this);

    @NotEmpty
    @ApiItem("partner_id")
    private String partner;

    public void doCheck() {

    }

    @Override
    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Override
    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
