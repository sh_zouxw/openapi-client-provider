/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_PAY_RESULT, type = ApiMessageType.Response)
public class QueryPayResultResponse extends SinapayResponse {

	/**
	 * 交易订单号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "out_pay_no")
	private String outPayNo;

	/**
	 * 交易状态
	 *
	 * 交易状态， 详见附录中的交易状态
	 */
	@NotEmpty
	@Size(max = 20)
	@ApiItem(value = "pay_status")
	private String payStatus;

	public String getOutPayNo() {
		return outPayNo;
	}

	public void setOutPayNo(String outPayNo) {
		this.outPayNo = outPayNo;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

}
