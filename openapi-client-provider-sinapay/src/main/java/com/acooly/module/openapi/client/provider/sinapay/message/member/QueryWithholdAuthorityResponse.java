/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月1日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.member.dto.AuthInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * 委托扣款签约 查询
 * 
 * 委托扣款签约的同步通知回来后根据identify查询结果
 * 
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_WITHHOLD_AUTHORITY, type = ApiMessageType.Response)
public class QueryWithholdAuthorityResponse extends SinapayResponse {

	/**
	 * 是否代扣授权
	 * 
	 * 是否代扣授权，Y：是；N：否
	 */
	@ApiItem(value = "is_withhold_authoity")
	private String isWithholdAuthoity;

	/**
	 * 授权类型
	 * 
	 * 授权的时间，未授权没有 格式yyyyMMddhhmmss
	 */
	@ApiItem(value = "withhold_authoity_time")
	private String withholdAuthoityTime;

	/**
	 * 用户标识信息
	 */
	@ApiItem(value = "auth_list")
	private List<AuthInfo> authList;

}
