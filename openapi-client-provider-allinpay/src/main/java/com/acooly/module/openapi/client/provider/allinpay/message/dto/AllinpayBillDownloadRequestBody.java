package com.acooly.module.openapi.client.provider.allinpay.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 19:12
 * @Description:
 */
@Getter
@Setter
@ToString
@XStreamAlias("QTRANSREQ")
public class AllinpayBillDownloadRequestBody implements Serializable {

    /**
     * 商户代码（通联通装莽，调用的时候不用传）
     */
    @Size(max = 15)
    @NotBlank
    @XStreamAlias("MERCHANT_ID")
    private String merchantId;


    /**
     * 状态
     * 交易状态条件, 0成功,1失败, 2全部
     */
    @Size(max = 2)
    @NotBlank
    @XStreamAlias("STATUS")
    private String status = "2";


    /**
     * 查询类型
     * 0.按完成日期1.按提交日期
     */
    @Size(max = 2)
    @NotBlank
    @XStreamAlias("TYPE")
    private String type = "0";

    /**
     * 是否包含手续费
     * 0.不需手续费，1.包含手续费
     * 空则默认为0
     */
    @Size(max = 2)
    @XStreamAlias("CONTFEE")
    private String contfee;

    /**
     * 开始日
     * YYYYMMDDHHmmss
     */
    @Size(max = 14)
    @NotBlank
    @XStreamAlias("START_DAY")
    private String startDay;

    /**
     * 结束日
     * YYYYMMDDHHmmss
     */
    @Size(max = 14)
    @NotBlank
    @XStreamAlias("END_DAY")
    private String endDay;

    /**
     * 结算账号
     */
    @Size(max = 30)
    @XStreamAlias("SETTACCT")
    private String settAcct;

}
