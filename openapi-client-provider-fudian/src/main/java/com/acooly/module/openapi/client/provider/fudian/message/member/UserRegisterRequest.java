/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-01-25 16:40 创建
 */
package com.acooly.module.openapi.client.provider.fudian.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianRequest;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhangpu 2018-01-25 16:40
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.USER_REGISTER, type = ApiMessageType.Request)
public class UserRegisterRequest extends FudianRequest {

    /**
     * 真实姓名
     */
    @NotBlank
    private String realName;

    /**
     * 证件类型
     */
    @NotBlank
    private String identityType = "1";

    /**
     * 证件号
     */
    @NotBlank
    private String identityCode;

    /**
     * 手机号
     */
    @NotBlank
    private String mobilePhone;

    /**
     * 角色类型
     * 角色类型：1借款人 3出借人
     */
    @NotBlank
    private String roleType = "1";
}
