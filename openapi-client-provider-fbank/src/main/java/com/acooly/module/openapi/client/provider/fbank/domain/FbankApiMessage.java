/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.fbank.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.fbank.support.FbankAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/** @author zhike */
@Getter
@Setter
public class FbankApiMessage implements ApiMessage {

  /** 接口名称 */
  private String service;

  /** 商户号 */
  @NotBlank(message = "商户号不能为空")
  private String mchntId;

  /** MD5签名结果 */
  @FbankAlias(value = "signature", sign = false)
  private String signature;

  /** 请求url */
  @FbankAlias(value = "gatewayUrl", sign = false, request = false)
  private String gatewayUrl;

  public void doCheck() {}

  @Override
  public String toString() {
    return ToString.toString(this);
  }

  @Override
  public String getService() {
    return service;
  }

  @Override
  public String getPartner() {
    return mchntId;
  }
}
