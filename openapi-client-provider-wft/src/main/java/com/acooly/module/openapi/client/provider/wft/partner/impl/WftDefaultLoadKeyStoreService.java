//package com.acooly.module.openapi.client.provider.wft.partner.impl;
//
//import com.acooly.core.utils.Strings;
//import com.acooly.module.openapi.client.provider.wft.OpenAPIClientWftProperties;
//import com.acooly.module.openapi.client.provider.wft.WftConstants;
//import com.acooly.module.safety.key.AbstractKeyLoadManager;
//import com.acooly.module.safety.key.KeySimpleLoader;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class WftDefaultLoadKeyStoreService extends AbstractKeyLoadManager<String> implements KeySimpleLoader {
//
//    @Autowired
//    private OpenAPIClientWftProperties openAPIClientWftProperties;
//
//    @Override
//    public String doLoad(String principal) {
//        if (Strings.equals("7551000001", principal)) {
//            return openAPIClientWftProperties.getPrivateKey();
//        } else {
//            //微信APP支付的时候使用
//            return "7daa4babae15ae17eee90c9e";
//        }
//    }
//
//    @Override
//    public String getProvider() {
//        return WftConstants.PROVIDER_NAME;
//    }
//}
