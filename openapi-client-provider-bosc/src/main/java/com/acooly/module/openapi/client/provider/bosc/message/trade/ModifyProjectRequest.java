package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscProjectStatusEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.MODIFY_PROJECT, type = ApiMessageType.Request)
public class ModifyProjectRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 标的号
	 */
	@NotEmpty
	@Size(max = 50)
	private String projectNo;
	/**
	 * 更新标的状态，见【标的状态】
	 */
	@NotNull
	private BoscProjectStatusEnum status;
	
	public ModifyProjectRequest () {
		setService (BoscServiceNameEnum.MODIFY_PROJECT.code ());
	}
	
	public ModifyProjectRequest (String requestNo, String projectNo,
	                             BoscProjectStatusEnum status) {
		this ();
		this.requestNo = requestNo;
		this.projectNo = projectNo;
		this.status = status;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
	
	public BoscProjectStatusEnum getStatus () {
		return status;
	}
	
	public void setStatus (BoscProjectStatusEnum status) {
		this.status = status;
	}
}