package com.acooly.module.openapi.client.provider.bosc;

import com.acooly.module.openapi.client.provider.bosc.message.fund.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
@Component
public class BoscFundApiService {
	
	@Resource(name = "boscApiServiceClient")
	private BoscApiServiceClient boscApiServiceClient;
	
	/**
	 * 提现重定向
	 * @param request
	 * @return
	 */
	public String withdrawRedirect(WithdrawRequest request){
		return boscApiServiceClient.redirectGet (request);
	}
	
	
	/**
	 * 充值重定向
	 * @param rechargeRequest
	 * @return
	 */
	public String rechargeRedirect(RechargeRequest rechargeRequest){
		return  boscApiServiceClient.redirectGet (rechargeRequest);
	}
	
	/**
	 * 批量交易
	 * @param request
	 * @return
	 */
	public AsyncTransactionResponse asyncTransaction(AsyncTransactionRequest request){
		return (AsyncTransactionResponse)boscApiServiceClient.execute (request);
	}
	
	/**
	 * 资金冻结
	 * @param request
	 * @return
	 */
	public FreezeResponse freeze(FreezeRequest request){
		return (FreezeResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 资金解冻
	 * @param request
	 * @return
	 */
	public UnfreezeResponse unfreeze(UnfreezeRequest request){
		return (UnfreezeResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 下载对账文件
	 * @param request
	 */
	public DownloadCheckFileResponse downloadCheckFile(DownloadCheckFileRequest request){
		return boscApiServiceClient.downloadFile (request);
	}
	
	/**
	 * 对账文件确认
	 * @param request
	 * @return
	 */
	public ConfirmCheckFileResponse confirmCheckFile(ConfirmCheckFileRequest request){
		return (ConfirmCheckFileResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 查询充值交易明细
	 * @param request
	 * @return
	 */
	public QueryTransactionRechargeResponse queryTransactionRecharge(QueryTransactionRequest request){
		return (QueryTransactionRechargeResponse) boscApiServiceClient.executeTransactionQuery (request);
	}
	
	/**
	 * 查询提现交易明细
	 * @param request
	 * @return
	 */
	public QueryTransactionWithdrawResponse queryTransactionWithdraw(QueryTransactionRequest request){
		return (QueryTransactionWithdrawResponse) boscApiServiceClient.executeTransactionQuery (request);
		
	}
	
	/**
	 * 查询冻结交易明细
	 * @param request
	 * @return
	 */
	public QueryTransactionFreezeResponse queryTransactionFreeze(QueryTransactionRequest request){
		return (QueryTransactionFreezeResponse) boscApiServiceClient.executeTransactionQuery (request);
	}
	
	/**
	 * 查询解冻交易明细
	 * @param request
	 * @return
	 */
	public QueryTransactionUnfreezeResponse queryTransactionUnfreeze(QueryTransactionRequest request){
		return (QueryTransactionUnfreezeResponse) boscApiServiceClient.executeTransactionQuery (request);
	}
	
	/**
	 * 查询批量交易确认明细
	 * @param request
	 * @return
	 */
	public QueryTransactionTransactionResponse queryTransaction(QueryTransactionRequest request){
		return (QueryTransactionTransactionResponse) boscApiServiceClient.executeTransactionQuery (request);
	}
	
	/**
	 * 提现拦截
	 * @param request
	 * @return
	 */
	public InterceptWithdrawResponse interceptWithdraw(InterceptWithdrawRequest request){
		return (InterceptWithdrawResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 提现审核拒绝
	 * @param request
	 * @return
	 */
	public CancelWithdrawResponse cancelWithdraw(CancelWithdrawRequest request){
		return (CancelWithdrawResponse) boscApiServiceClient.execute (request);
	}
	
	/**
	 * 提现审核通过
	 * @param request
	 * @return
	 */
	public ConfirmWithdrawResponse confirmWithdraw(ConfirmWithdrawRequest request){
		return (ConfirmWithdrawResponse) boscApiServiceClient.execute (request);
	}
}
