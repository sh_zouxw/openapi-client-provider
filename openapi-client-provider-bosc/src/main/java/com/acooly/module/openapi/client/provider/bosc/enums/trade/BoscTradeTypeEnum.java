/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.bosc.enums.trade;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * BOSC交易类型
 *
 * @author zhangpu 2017-09-24 19:29
 */
public enum BoscTradeTypeEnum implements Messageable {
	
	TENDER ("TENDER", "投标"),
	
	REPAYMENT ("REPAYMENT", "还款"),
	
	CREDIT_ASSIGNMENT ("CREDIT_ASSIGNMENT", "债权认购"),
	
	COMPENSATORY ("COMPENSATORY", "直接代偿"),
	
	INDIRECT_COMPENSATORY ("INDIRECT_COMPENSATORY", "间接代偿"),
	
	PLATFORM_INDEPENDENT_PROFIT ("PLATFORM_INDEPENDENT_PROFIT", "独立分润"),
	
	MARKETING ("MARKETING", "平台营销款"),
	
	PLATFORM_SERVICE_DEDUCT ("PLATFORM_SERVICE_DEDUCT", "收费"),
	
	FUNDS_TRANSFER ("FUNDS_TRANSFER", "平台资金划拨"),;
	
	private final String code;
	private final String message;
	
	private BoscTradeTypeEnum (String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public String getCode () {
		return code;
	}
	
	public String getMessage () {
		return message;
	}
	
	public String code () {
		return code;
	}
	
	public String message () {
		return message;
	}
	
	public static Map<String, String> mapping () {
		Map<String, String> map = Maps.newLinkedHashMap ();
		for (BoscTradeTypeEnum type : values ()) {
			map.put (type.getCode (), type.getMessage ());
		}
		return map;
	}
	
	/**
	 * 通过枚举值码查找枚举值。
	 *
	 * @param code 查找枚举值的枚举值码。
	 *
	 * @return 枚举值码对应的枚举值。
	 * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
	 */
	public static BoscTradeTypeEnum find (String code) {
		for (BoscTradeTypeEnum status : values ()) {
			if (status.getCode ().equals (code)) {
				return status;
			}
		}
		throw new IllegalArgumentException ("BoscServiceNameEnum not legal:" + code);
	}
	
	/**
	 * 获取全部枚举值。
	 *
	 * @return 全部枚举值。
	 */
	public static List<BoscTradeTypeEnum> getAll () {
		List<BoscTradeTypeEnum> list = new ArrayList<BoscTradeTypeEnum> ();
		for (BoscTradeTypeEnum status : values ()) {
			list.add (status);
		}
		return list;
	}
	
	/**
	 * 获取全部枚举值码。
	 *
	 * @return 全部枚举值码。
	 */
	public static List<String> getAllCode () {
		List<String> list = new ArrayList<String> ();
		for (BoscTradeTypeEnum status : values ()) {
			list.add (status.code ());
		}
		return list;
	}
	
}
