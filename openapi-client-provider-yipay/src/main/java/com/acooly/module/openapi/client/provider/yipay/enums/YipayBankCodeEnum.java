/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.yipay.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by liubin@prosysoft.com on 2017/9/27.
 */
public enum YipayBankCodeEnum implements Messageable {

    BANK_CODE_865500("865500","上海银行"),
    BANK_CODE_865600("865600","深圳发展银行"),
    BANK_CODE_865700("865700","平安银行"),
    BANK_CODE_865800("865800","华夏银行"),
    BANK_CODE_865900("865900","北京银行"),
    BANK_CODE_866000("866000","中国邮政储蓄银行"),
    BANK_CODE_866100("866100","中国银行"),
    BANK_CODE_866200("866200","中国工商银行"),
    BANK_CODE_866300("866300","中国农业银行"),
    BANK_CODE_866400("866400","交通银行"),
    BANK_CODE_866500("866500","中国建设银行"),
    BANK_CODE_866600("866600","中国民生银行"),
    BANK_CODE_866700("866700","广州市农村信用社"),
    BANK_CODE_866800("866800","广东发展银行"),
    BANK_CODE_866900("866900","招商银行"),
    BANK_CODE_867000("867000","广州银行"),
    BANK_CODE_867100("867100","浦东发展银行"),
    BANK_CODE_867200("867200","光大银行"),
    BANK_CODE_867300("867300","广东农信"),
    BANK_CODE_867302("867302","佛山顺德农村   商业银行"),
    BANK_CODE_867303("867303","江门市新会区农村商业银行"),
    BANK_CODE_867304("867304","南海农村信用社"),
    BANK_CODE_867305("867305","深圳农村商业银行"),
    BANK_CODE_867306("867306","甘肃省农村信用社联合社"),
    BANK_CODE_867307("867307","北京农村商业银行"),
    BANK_CODE_867308("867308","江西农信"),
    BANK_CODE_867319("867319","东莞农信"),
    BANK_CODE_867400("867400","中信银行"),
    BANK_CODE_867600("867600","兴业银行"),
    BANK_CODE_867800("867800","东莞农商"),
    BANK_CODE_867900("867900","东莞银行"),
    BANK_CODE_867901("867901","珠海华润银行"),
    BANK_CODE_867902("867902","广东南粤银行"),
    BANK_CODE_867903("867903","兰州银行"),
    BANK_CODE_867905("867905","长沙银行"),
    BANK_CODE_867907("867907","成都银行"),
    BANK_CODE_868025("868025","威海市商业银行"),
    BANK_CODE_868032("868032","张家港农商银行"),
    BANK_CODE_868024("868024","东营市商业银行"),
    BANK_CODE_868064("868064","宁夏银行"),
    BANK_CODE_868000("868000","苏州银行"),
    BANK_CODE_868054("868054","重庆农商行"),
    BANK_CODE_868030("868030","上饶银行"),
    BANK_CODE_868046("868046","乌鲁木齐市商业银行"),
    BANK_CODE_867320("867320","江苏省农村信用社联合"),
    BANK_CODE_868019("868019","齐商银行"),
    BANK_CODE_868063("868063","富滇银行"),
    BANK_CODE_868055("868055","河北银行"),
    BANK_CODE_868051("868051","漯河市商业银行"),
    BANK_CODE_868022("868022","泰安市商业银行"),
    BANK_CODE_868031("868031","广西北部湾银行"),
    BANK_CODE_868091("868091","徽商银行"),
    BANK_CODE_868062("868062","汉口银行"),
    BANK_CODE_868098("868098","尧都信用合作联社"),
    BANK_CODE_868097("868097","晋城市商业银行"),
    BANK_CODE_868096("868096","温州市商业银行"),
    BANK_CODE_868035("868035","浙商银行"),
    BANK_CODE_868087("868087","江苏银行"),
    BANK_CODE_868100("868100","支付宝"),
    BANK_CODE_868099("868099","翼支付账户"),
    BANK_CODE_868102("868102","天翼积点劵"),
    BANK_CODE_868101("868101","财付通账户"),
    BANK_CODE_868095("868095","银联在线支付"),
    BANK_CODE_868015("868015","大连银行"),
    BANK_CODE_868040("868040","宁波银行"),
    BANK_CODE_868088("868088","南京银行"),
    BANK_CODE_868094("868094","东亚银行"),
    BANK_CODE_868009("868009","渤海银行"),
    BANK_CODE_868093("868093","上海农村商业银行"),
    BANK_CODE_868034("868034","杭州银行"),
    BANK_CODE_867322("867322","珠海市农村信用社"),
    BANK_CODE_868026("868026","日照银行"),
    ;

    private final String code;
    private final String message;

    private YipayBankCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (YipayBankCodeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static YipayBankCodeEnum find(String code) {
        for (YipayBankCodeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("FudianAuditStatusEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<YipayBankCodeEnum> getAll() {
        List<YipayBankCodeEnum> list = new ArrayList<YipayBankCodeEnum>();
        for (YipayBankCodeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (YipayBankCodeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
