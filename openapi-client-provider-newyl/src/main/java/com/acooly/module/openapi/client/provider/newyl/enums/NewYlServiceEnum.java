/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.newyl.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 服务名称枚举
 *
 * @author
 */
public enum NewYlServiceEnum implements Messageable {

    NEW_YL_REAL_DEDUCT("newYlRealDeduct","100004", "新银联单笔代扣"),
    NEW_YL_BATCH_DEDUCT("newYlBatchDeduct","100001", "新银联批量代扣"),
    NEW_YL_DEDUCT_QUERY("newYlDeductQuery","200001", "新银联代扣结果查询"),
    ;
    private final String code;
    private final String key;
    private final String message;

    private NewYlServiceEnum(String code, String key, String message) {
        this.code = code;
        this.message = message;
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public String getKey() {
        return key;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (NewYlServiceEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static NewYlServiceEnum find(String code) {
        for (NewYlServiceEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param key 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
     */
    public static NewYlServiceEnum findByKey(String key) {
        for (NewYlServiceEnum status : values()) {
            if (status.getKey().equals(key)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<NewYlServiceEnum> getAll() {
        List<NewYlServiceEnum> list = new ArrayList<NewYlServiceEnum>();
        for (NewYlServiceEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (NewYlServiceEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }


}
