/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.weixin.domain;

import com.acooly.module.openapi.client.provider.weixin.support.WeixinAlias;

import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 * @author zhangpu
 */
@Getter
@Setter
public class WeixinResponse extends WeixinApiMessage {

	/** 通讯状态code */
	@WeixinAlias(value = "return_code")
	private String returnCode;
	
	/** 返回信息 */
	@WeixinAlias(value = "returnMsg")
	private String returnMsg;
	
	// 以下信息在 returnCode = SUCCESS 才有
	
	/** 随机字符串 */
	@WeixinAlias(value = "nonce_str")
	private String nonceStr;
	
	/** 签名 */
	@WeixinAlias(value = "sign")
	private String sign;
	
	/** 业务结果 */
	@WeixinAlias(value = "result_code")
	private String resultCode;
	
	/** 错误code */
	@WeixinAlias(value = "err_code")
	private String errCode;
	
	/** 错误描述 */
	@WeixinAlias(value = "err_code_des")
	private String errCodeDes;
}
