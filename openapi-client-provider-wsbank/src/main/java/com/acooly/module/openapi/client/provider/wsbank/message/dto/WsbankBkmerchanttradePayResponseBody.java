package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 15:32
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankBkmerchanttradePayResponseBody implements Serializable {

	/**
	 * 返回码组件。当ResultStatus=S时才有后续的参数返回。
	 */
	@XStreamAlias("RespInfo")
	private WsbankResponseInfo responseInfo;

	/**
	 * 网商支付订单号。明确支付成功一定有
	 */
	@XStreamAlias("OrderNo")
	private String orderNo;

//	/**
//	 * 第三方支付授权码。扫码支付授权码，设备读取用户支付宝或微信中的条码或者二维码信息
//	 */
//	@Size(max = 64)
//	@XStreamAlias("AuthCode")
//	@NotBlank
//	private String authCode;

	/**
	 * 外部交易号。由合作方系统生成，只能包含字母、数字、下划线；需保证合作方系统不重复。
	 */
	@Size(max = 64)
	@XStreamAlias("OutTradeNo")
	@NotBlank
	private String outTradeNo;

	/**
	 * 付款银行。银行类型，仅使用微信支付时有返回值。详细列表参看附录3。
	 */
	@Size(max = 16)
	@XStreamAlias("BankType")
	private String bankType;

	/**
	 * 支付宝或微信端的订单号，可用于打印小票给客户核对
	 */
	@Size(max = 64)
	@XStreamAlias("PayChannelOrderNo")
	private String payChannelOrderNo;

	/**
	 * 支付完成时间
	 */
	@XStreamAlias("GmtPayment")
	private String gmtPayment;

	/**
	 * 交易总额度。货币最小单位，人民币：分
	 */
	@XStreamAlias("TotalAmount")
	@NotBlank
	private String totalAmount;

	/**
	 * 币种。默认CNY。
	 */
	@XStreamAlias("Currency")
	@NotBlank
	private String currency;

	/**
	 * 合作方机构号（网商银行分配）
	 */
	@Size(max = 64)
	@XStreamAlias("IsvOrgId")
	@NotBlank
	private String isvOrgId;

	@Size(max = 64)
	@XStreamAlias("MerchantId")
	@NotBlank
	private String merchantId;

	/**
	 * 支付渠道类型。该笔支付走的第三方支付渠道。可选值： ALI：支付宝 WX：微信支付 QQ：手机QQ（暂未开放） JD：京东钱包（暂未开放）
	 */
	@Size(max = 64)
	@XStreamAlias("ChannelType")
	@NotBlank
	private String channelType;

	/**
	 * 商户订单号。该订单号与支付宝、微信支付客户端账单详情显示的商户订单号一致，通过该订单后可发起扫码退款或查询交易。
	 */
	@Size(max = 64)
	@XStreamAlias("MerchantOrderNo")
	private String merchantOrderNo;

	/**
	 * 子商户appid。仅微信返回。 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("SubAppId")
	@Size(max = 64)
	private String subAppId;

	/**
	 * 现金券金额。仅微信返回，现金券支付金额<=订单总金额，订单总金额-现金券金额为现金支付金额。 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("CouponFee")
	private String couponFee;

	/**
	 * 是否关注公众账号，非必填，用户是否关注公众账号，仅微信返回。可选值： 1：关注 2：未关注，仅在公众账号类型支付有效，仅微信交易返回。
	 * 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("IsSubscribe")
	private String isSubscribe;

	/**
	 * 用户在商户 appid下的唯一标识。若商户使用合作机构的appid，则返回合作机构下的用户唯一标识。 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("SubOpenId")
	@NotBlank
	@Size(max = 128)
	private String subOpenId;

	/**
	 * 用户在银行 appid 下的唯一标识。仅微信返回。 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("OpenId")
	@NotBlank
	@Size(max = 128)
	private String openId;

	/**
	 * 附加信息，原样返回。
	 */
	@XStreamAlias("Attach")
	@Size(max = 128)
	private String attach;

	/**
	 * 买家支付宝登录账号。 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("BuyerLogonId")
	@Size(max = 128)
	private String buyerLogonId;

	/**
	 * 买家支付宝用户id。 该字段2017年9月30日提供线上服务。
	 */
	@Size(max = 128)
	@XStreamAlias("BuyerUserId")
	private String buyerUserId;

	/**
	 * 借贷标识。可选值： credit：信用卡 pcredit：花呗（仅支付宝） debit：借记卡 balance：余额 unknown：未知
	 * 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("Credit")
	private String credit;

	/**
	 * 实收金额，商户实际入账的金额（扣手续费之前）。仅支付宝返回。该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("ReceiptAmount")
	private String receiptAmount;

	/**
	 * 用户实付金额，建议打印在小票上避免退款时出现纠纷。仅支付宝返回。该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("BuyerPayAmount")
	private String buyerPayAmount;

	/**
	 * 开票金额，快速告知商户应该给用户开多少钱发票。仅支付宝返回。该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("InvoiceAmount")
	private String invoiceAmount;

}
