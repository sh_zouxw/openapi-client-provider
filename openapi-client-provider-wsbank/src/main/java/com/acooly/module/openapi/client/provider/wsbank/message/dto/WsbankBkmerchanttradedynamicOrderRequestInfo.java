package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("request")
public class WsbankBkmerchanttradedynamicOrderRequestInfo implements Serializable {

	private static final long serialVersionUID = 9042417282477178943L;

	/**
     * 请求报文头
     */
    @XStreamAlias("head")
    @NotNull
    private WsbankHeadRequest headRequest;

    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    @NotNull
    private WsbankBkmerchanttradedynamicOrderRequestBody requestBody;
}
