package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankResponse;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankBkmerchanttradePayResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author weili 2018/5/22 15:32
 */
@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.BKMERCHANTTRADE_PAY,type = ApiMessageType.Response)
public class WsbankBkmerchanttradePayResponse extends WsbankResponse {

    /**
     * 响应报文信息
     */
    @XStreamAlias("response")
    private WsbankBkmerchanttradePayResponseInfo responseInfo;
}
