package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankNotify;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankPrePayNoticeNotifyInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/24 14:04
 */
@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.PRE_PAY_NOTICE,type = ApiMessageType.Notify)
public class WsbankPrePayNoticeNotify extends WsbankNotify {

    /**
     * 线下支付结果异步通知
     */
    @XStreamAlias("request")
    private WsbankPrePayNoticeNotifyInfo requestInfo;
}
