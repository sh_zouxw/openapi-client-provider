package com.acooly.module.openapi.client.provider.baofu.message.info;

import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuInfoMessage;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/1/30 9:45
 */
@Getter
@Setter
@XStreamAlias("order_info")
public class BaoFuBatchDeductSigleQueryResInfo extends BaoFuInfoMessage {

    /**
     * 订单日期 14 位定长。
     * 格式：年年年年月月日日时时分分秒秒
     */
    @NotBlank
    @XStreamAlias("trade_date")
    private String tradeDate;


    /**
     * 商户订单号
     *
     */
    @XStreamAlias("trans_id")
    private String transId;

    /**
     * 批次号 宝付提供给商户的宝付批次编号
     *
     */
    @XStreamAlias("batch_id")
    private String batchId;

    /**
     * 订单状态 I-处理中 S-成功 F-失败
     *
     */
    @XStreamAlias("order_stat")
    private String orderStatus;

    /**
     * 返回状态码
     */
    @XStreamAlias("resp_code")
    private String respCode;

    /**
     * 返回信息
     */
    @XStreamAlias("resp_msg")
    private String respMsg;

    /**
     * 交易描述
     */
    @XStreamAlias("trans_no")
    private String transNo;

    /**
     * 成功金额
     */
    @BaoFuAlias(value = "succ_amt")
    @XStreamAlias("order_info")
    private String succAmt;

    /**
     * 商户保留域
     */
    @XStreamAlias("req_reserved")
    private String reqReserved;

    /**
     * 商户批次号 商户请求时提供的批次编号
     */
    @XStreamAlias("trans_batch_id")
    private String transBatchId;
}
